<?php
/**
* Plugin Name: MMD Vertical Tabs Element
* Plugin URI: https://MacMillanDesign.com
* Description: Vertical tab element for JumpStart Layout Builder.
* Version: 1.0.0
* Author: Lilian Patino and Matthew MacMillan
* License: GPL2
*/

require_once( 'include/include.php' );

if ( is_admin() ) {
  require_once( 'admin/tab-admin.php' );
  
} else {
  require_once( 'public/print-tab.php' );
}

/*function class_tab() {
	  echo '<style>
	   div.tab {
	    float: left;
	    border: 1px solid #ccc;
	    background-color: #f1f1f1;
	    width: 30%;
	    height: 300px;
	}

	// Style the buttons inside the tab 
	div.tab button {
	    display: block;
	    background-color: inherit;
	    color: black;
	    padding: 22px 16px;
	    width: 100%;
	    border: none;
	    outline: none;
	    text-align: left;
	    cursor: pointer;
	    transition: 0.3s;
	    font-size: 17px;
	}

	// Change background color of buttons on hover 
	div.tab button:hover {
	    background-color: #ddd;
	}

	// Create an active/current "tab button" class 
	div.tab button.active {
	    background-color: #ccc;
	}

	// Style the tab content 
	.tabcontent {
	    float: left;
	    padding: 0px 12px;
	    border: 1px solid #ccc;
	    width: 70%;
	    border-left: none;
	    height: 300px;
	}
	  </style>';
}
add_action( 'after_setup_theme', 'class_tab', 11 );


function wptuts_scripts_basic()
{
echo '<script>
	function openCity(evt, cityName) {
	    var i, tabcontent, tablinks;
	    tabcontent = document.getElementsByClassName("tabcontent");
	    for (i = 0; i < tabcontent.length; i++) {
	        tabcontent[i].style.display = "none";
	    }
	    tablinks = document.getElementsByClassName("tablinks");
	    for (i = 0; i < tablinks.length; i++) {
	        tablinks[i].className = tablinks[i].className.replace(" active", "");
	    }
	    document.getElementById(cityName).style.display = "block";
	    evt.currentTarget.className += " active";
	}

	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
	</script>';
}
add_action( 'wp_footer', 'wptuts_scripts_basic', 11 );

add_action('after_setup_theme', 'foo');
function foo() {
$options =  array(
        'subgroup_start' => array(
            'type'    => 'subgroup_start',
            'class'   => 'tabs'
          ),
          'setup' => array(
          'id'    => 'setup',
          'name'    => __( 'Setup Tabs', 'theme-blvd-layout-builder' ),
          'desc'    => __( 'Choose the number of tabs along with inputting a name for each one. These names are what will appear on the actual tab buttons across the top of the tab set.', 'theme-blvd-layout-builder' ),
          'type'    => 'tabs'
        ),
        'height' => array(
          'id'    => 'height',
          'name'    => __( 'Fixed Height', 'theme-blvd-layout-builder' ),
          'desc'    => __( 'Apply automatic fixed height across all tabs.<br><br>This just takes the height of the tallest tab\'s content and applies that across all tabs. This can help with "page jumping" in the case that not all tabs have equal amount of content. It can also help in the case when you\'re getting unwanted scrollbars on the inner content areas of tabs.', 'theme-blvd-layout-builder' ),
          'std'   => 1,
          'type'    => 'checkbox'
        ),
        'tab_1' => array(
          'id'    => 'tab_1',
          'name'    => __( 'Tab #1 Content', 'theme-blvd-layout-builder' ),
          'desc'    => __( 'Configure the content for the first tab.', 'theme-blvd-layout-builder' ),
          'type'    => 'content',
          'options' => array( 'page', 'raw', 'widget' )
        ),
        'subgroup_end' => array(
            'type'    => 'subgroup_end'
          )
      );
	$args = array(
	    'id'		=> 'profile_box',
	    'name'		=> 'Profile Box',
	    'options'	=> $options,
	    'callback'	=> 'my_profile_box',
	    'hook' => 'themeblvd_tabs',
	    'shortcode' => '[tabs]'
	);
	themeblvd_add_builder_element($args);
}

function my_profile_box( $id, $options ) {
	$i=0;
	$ouput="";
	$ouput	= '<div class="tab">';
	foreach ($options['setup'] as $val){
		if ($i==0) {
				$ouput.='<button id="defaultOpen" class="tablinks" onclick="openCity(event, \''.$i.'\')">'.$val['title'].'</button>';
		}else{
				$ouput.='<button class="tablinks" onclick="openCity(event, \''.$i.'\')">'.$val['title'].'</button>';
		}
		$i.=$i+1;
	}
	$ouput.= '</div>';

	$i=0;

	foreach ($options['setup'] as $val){
		$ouput.= '<div id="'.$i.'" class="tabcontent">';
		$ouput.='<h3>'.$val['title'].'</h3>';
		$ouput.='<p>'.$val['content']['raw'].'</p>';
		$ouput.= '</div>';
		$i.=$i+1;
	}
	
	echo $ouput;




  	//foreach ($options['setup'] as $val)
  		//	echo $val['title'] . " - ".  $val['content']['raw'] . "<br>";


} */


?>