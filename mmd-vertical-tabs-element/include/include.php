<?php


add_action('after_setup_theme', 'v_tabs_element');
function v_tabs_element() {
$options =  array(
        'subgroup_start' => array(
            'type'    => 'subgroup_start',
            'class'   => 'tabs'
          ),
          'setup' => array(
          'id'    => 'setup',
          'name'    => __( 'Setup Tabs', 'theme-blvd-layout-builder' ),
          'desc'    => __( 'Choose the number of tabs along with inputting a name for each one. These names are what will appear on the actual tab buttons across the top of the tab set.', 'theme-blvd-layout-builder' ),
          'type'    => 'tabs'
        ),
        'height' => array(
          'id'    => 'height',
          'name'    => __( 'Fixed Height', 'theme-blvd-layout-builder' ),
          'desc'    => __( 'Apply automatic fixed height across all tabs.<br><br>This just takes the height of the tallest tab\'s content and applies that across all tabs. This can help with "page jumping" in the case that not all tabs have equal amount of content. It can also help in the case when you\'re getting unwanted scrollbars on the inner content areas of tabs.', 'theme-blvd-layout-builder' ),
          'std'   => 1,
          'type'    => 'checkbox'
        ),
        'tab_1' => array(
          'id'    => 'tab_1',
          'name'    => __( 'Tab #1 Content', 'theme-blvd-layout-builder' ),
          'desc'    => __( 'Configure the content for the first tab.', 'theme-blvd-layout-builder' ),
          'type'    => 'content',
          'options' => array( 'raw')
        ),
        'subgroup_end' => array(
            'type'    => 'subgroup_end'
          )
      );
	$args = array(
	    'id'		=> 'mmd-tab',
	    'name'		=> 'MMD Tab',
	    'options'	=> $options,
	    'callback'	=> 'mmd_tab_call',
	    'hook' => 'themeblvd_tabs',
	    'shortcode' => '[tabs]'
	);
	themeblvd_add_builder_element($args);
}

function mmd_tab_call( $id, $options ) {
	$i=0;
	$output="";
	$output	= '<div class="tab">';
	foreach ($options['setup'] as $val){
		if ($i==0) {
				$output.='<button id="defaultOpen" class="tablinks" onclick="openTab(event, \''.$i.'\')">'.$val['title'].'</button>';
		}else{
				$output.='<button class="tablinks" onclick="openTab(event, \''.$i.'\')">'.$val['title'].'</button>';
		}
		$i.=$i+1;
	}
	$output.= '</div>';

	$i=0;

	foreach ($options['setup'] as $val){
		$output.= '<div id="'.$i.'" class="tabcontent" style="display: none;">';
		$output.='<h3>'.$val['title'].'</h3>';
		$output.='<p>'.$val['content']['raw'].'</p>';
		$output.= '</div>';
		$i.=$i+1;
	}
	
	//echo esc_html($output);
	echo $output;

} 


?>